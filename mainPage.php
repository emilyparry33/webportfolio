<?php
header("Content-Type: text/html; charset=utf-8");
$hostname = $_SERVER["HTTP_HOST"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  <! Icons for SocialMedia >
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/f1296f1053.js"></script>

        <title>Emily Parry Portfolio</title>
   
       
</head>
<body>
     
    <div class="container-fluid" id="NavBar">
   
        <div class="row">
            <div class="col-md-4 col-xs-4">
                
    
            
                    <a href="https://www.linkedin.com/in/emily-parry-b95b76150/" class="fab fa-linkedin fa-3x"></a>
           
                    <a id="instagram"  href="https://www.instagram.com/emily_parry_3d/"class="fab fa-instagram fa-3x"></a>
             </div>
            <div class="col-md-4 col-xs-4">
                    <h3 id="Title">Emily Parry</h3>
            </div>
                <div class="col-md-4 col-xs-4" align="right" >
                    <a href="#Projects" style="font-family: 'Indie Flower', cursive; color: white; font-size: 20px;">Projects</a>
                    <a href="#About" style="font-family: 'Indie Flower', cursive; color: white; font-size: 20px;">About</a>
                 <a id="homeButton" href="#" class="fa fa-home fa-2x" ></a> 
                </div>
            </div>
    </div>
    
    <div class="section" id="MainBody">
        <!-- Clouds -->
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-3 col-xs-3">
                    <img class="cloud" src="images/Frontpage/Clouds.png"> 
                </div>
            </div>
             <div class="row">
                     <div class="col-md-3 col-xs-3">
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <img class="cartoonwave" src="images/Frontpage/balloon.gif">
                    <!--  <img class="cloud" src="images/Frontpage/Clouds.png">  -->

                    </div> 
                    <div class="col-md-3 col-xs-3">
                        <img class="pixelspeechbubble" src="images/Frontpage/pixel-speech-bubble.gif">
                    </div>
                     <div class="col-md-3 col-xs-3">
                    </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-3">
                </div>
                <div class="col-md-3 col-xs-3">
                </div>
                <div class="col-md-3 col-xs-3">
                </div>
                <div class="col-md-3 col-xs-3">
                      <img class="cloud" src="images/Frontpage/Clouds.png"> 
                </div>
            </div>
        </div>
        
     
      <!--
   <div class="container-fluid">
                 <div class="row" style="padding-bottom : 50px;">
                      <div class="col-md-4 col-xs-4">
                        
                     </div>
                   
                     <div class="col-md-4 col-xs-4">
                          <h1 style="color:#ffffff; font-size:100px"> About Me</h1>
                     </div>
                     <div class="col-md-4 col-xs-4">
                         
                     </div>
                </div>
          
            <div class="row justify-content-center"  style="padding-bottom : 200px;">
                <div class="col-md-4 col-xs-4" align="center" >
                  
                        <i class="fas fa-laptop-code fa-4x" style="align:center;"></i>
                        <p style="color:white; font-size: 30px;">Web Designer/Developer</p>
             
                </div>
                 <div class="col-md-4 col-xs-4" align="center">
                   
                        <i class="fas fa-university fa-4x"></i>
                        <p style="color:white; font-size: 30px;">BSc.(Hons.)Computing in Games Development</p>
                </div>
                <div class="col-md-4 col-xs-4" align="center">
                        <i class="fas fa-globe-europe fa-4x"></i>
                        <p style="color:white; font-size: 30px;">Based in Ireland</p>
                </div>
            </div>
  
        </div>  -->
         <div class="container-fluid" id="About"> 
              <div class="row" style="padding-bottom : 50px;">
                   
               <div class="col-md-12 col-xs-12" align="center">
                          <h1 style="color:white; font-size:100px"> About Me</h1>
                          <p style="font-size: 40px; color:white;">I have a passion for creating fun and compelling websites. </p>
                     </div>
             </div>    
                <div class="row justify-content-center"  style="padding-bottom : 200px; margin-top:50px;">
                <div class="col-md-4 col-xs-4" align="center" >
                  
                        <i class="fas fa-laptop-code fa-4x" style="align:center;"></i>
                        <p style="color:white; font-size: 30px;">Web Designer/Developer</p>
             
                </div>
                 <div class="col-md-4 col-xs-4" align="center">
                   
                        <i class="fas fa-university fa-4x"></i>
                        <p style="color: white; font-size: 30px;">BSc.(Hons.)Computing in Games Development</p>
                </div>
                <div class="col-md-4 col-xs-4" align="center">
                        <i class="fas fa-globe-europe fa-4x"></i>
                        <p style="color:white; font-size: 30px;">Based in Ireland</p>
                </div>
        </div>

            </div>
</div>
   


   
    <div class="Sand" id="Projects">
        <div class="container-fluid">
            <div class="row" align="center">
                <h3 style=" font-family: 'Quicksand', sans-serif; font-size:100px; color:white;">Coming soon</h3>
            </div>
            <div class="row">
               <div class="col-md-4 col-xs-4">
                   <img class="hut" src="images/Frontpage/blueHut.png">
                </div> 
                <div class="col-md-4 col-xs-4">
                   <img class="hut" src="images/Frontpage/pinkHut.png">
                </div> 
                <div class="col-md-4 col-xs-4">
                   <img class="hut" src="images/Frontpage/greenHut.png">
                </div>
            </div>
            
        
        </div>
    </div>

    
   
  <div class="section2">
         <div class="wave">
             
         </div>
    </div> 
   <footer style="background-color:white;">
        <div class="container" align="center">
          
                <h5 style="color:#82d1b3;">Contact me</h5>
                <p style="color:#82d1b3;">emilyparry0396@gmail.com</p>
       
           <span style="color:#82d1b3;">&copy; 2019 EmilyParry.</span>
       </div>  

</footer>
<!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>
</html>


